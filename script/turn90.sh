#!/bin/bash
if [[ `xrandr -q | grep 1280x800+0+0` ]] ; then
    xrandr -o left && xsetwacom --set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" Rotate CCW    
else
    xrandr -o normal && xsetwacom --set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" Rotate NONE
fi
