#!/bin/bash
if [[ `xrandr -q | grep '1280x800+0+0 ('` ]] ; then
    xrandr -o inverted && xsetwacom --set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" Rotate half
else
    xrandr -o normal && xsetwacom --set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" Rotate none
fi
