#Require Git
REPO="$HOME/Git/guiX200t"
repo=("emacs" "script")
dest=("$HOME/.emacs.d" "$HOME/.local/share/script")

if [ ! -d "$REPO" ] ; then #clone repo
    mkdir $HOME/Git
    mkdir $REPO 
    git clone http://gitlab.com/mandraker/guiX200t.git $REPO
    cd $REPO && git rev-parse HEAD > ../.cacheGuiX200t && cd #init cache 
    echo
fi

# Any change on git?
hmain="$(cat $HOME/Git/.cacheGuiX200t)"
horigin="$(cd $REPO &&  git rev-parse origin && cd)"
if [ "$hmain" = "$horigin" ] && ! [ "$1" = "-f" ]; then #initial check
    echo "No change on repo"
    echo "If you want force a control try: -f as option"
elif [ -d "$REPO" ]; then 
    for ((i=0; i<${#repo[@]}; i++)); do
	if [ ! -d "${dest[$i]}" ]; then #dir not exist
	    echo "Create dir for ${repo[$i]}"
	    mkdir ${dest[$i]}
	    cp $REPO/${repo[$i]}/* ${dest[$i]}
	else # dir already exist
	    echo "Copy new ${repo[$i]}"
	    cp $REPO/${repo[$i]}/* ${dest[$i]}
	fi
    done
fi
